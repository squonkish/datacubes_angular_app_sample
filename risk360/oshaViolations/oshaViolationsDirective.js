/* global angular */
(function () {
  'use strict';

  angular
    .module('mvpUiApp')
    .directive('oshaViolationsSummary', function () {
      return {
        templateUrl: 'app/underwriting/risk360/oshaViolations/oshaViolationsTemplate.html',
        restrict: 'E',
        controller: 'oshaViolationsCtrl',
        scope: {}
      };
    });
})();