/* global angular */
(function () {
  'use strict';

  angular.module('mvpUiApp')
  .controller('oshaViolationsCtrl', ['$scope', '$rootScope', '$filter', 'Risk360', 'riskAnalysisModalOptions', 'errorService',
    function($scope, $rootScope, $filter, Risk360, riskAnalysisModalOptions, errorService) {
      $scope.modalOptions = riskAnalysisModalOptions;

      Risk360.getOverview($rootScope.getIntakeId(), $rootScope.getCompanyId())
        .then(function (data) {
          if (typeof data.overview != "undefined" &&
            data.overview !== null &&
            typeof data.overview.osha_violations_qtr != "undefined" &&
            data.overview.osha_violations_qtr !== null) {
              var vio = data.overview.osha_violations_qtr;
              $scope.vio_total = 0;

              vio.forEach(function(vio){
                $scope.vio_total += vio.count;
              });

            }

          if (data.overview && data.overview.osha_accidents_total) {
            $scope.osha_accidents_total = data.overview.osha_accidents_total;
          }
        })
        .catch(function (err) {
          errorService.logError(err);
        });

      Risk360.getViolationsData()
        .then(function (data) {
          $scope.data = data;

          $scope.options = {
            chart: {

              focusEnable: false,
              type: 'linePlusBarChart',
              height: 350,
              margin : {
                top: 80,
                right: 100,
                bottom: 50,
                left: 60
              },
              bars: {
                forceY: [0]
              },
              color: ['#00B7EC','#8064B6'],
              duration: 500,
              interpolate: 'cardinal',
              tension: 0.4,
              stacked: false,
              showControls: false,
              x: function(d, i){
                return i;
              },
              y: function(d){return d[2];},
              xAxis: {
                tickFormat: function(d){
                  var year = $scope.data[0].values[d] && $scope.data[0].values[d][1]|| 0;
                  var quarter = $scope.data[0].values[d] && $scope.data[0].values[d][0]|| 1;
                  var dx = "Q" + quarter;
                  dx += '-';
                  dx += year;
                  return dx;
                }
              },
              yAxis: {
                tickFormat: function(d){
                  return d;
                }
              },
              tooltips:true,
              tooltip: {
                contentGenerator: function (d) {
                  var quarter, cost, name;
                  if ( typeof d.point && d.point ) {
                    quarter = 'Q' + d.point[0];
                    quarter += '-';
                    quarter += d.point[1];
                    cost = d.point[2];
                    name = 'Penalties';
                    return '<p>' + quarter + '</p>' +
                    '<div style="padding:5px 10px;">' + '<svg height="10" width="15"><g><rect x= "5" y="0" width="10" height="10" fill ="' +
                    d.series[0].color + '"></rect></g></svg>' +
                    ' ' + name + ' : ' + $filter('currency')(cost,'$', 0) + '</div>';
                  }
                  else if ( typeof d.data && d.data ) {
                    quarter = 'Q' + d.data[0];
                    quarter += '-';
                    quarter += d.data[1];
                    var count = d.data[2];
                    name = 'Violations';
                    return '<p>' + quarter + '</p>' +
                    '<div style="padding:5px 10px;">' + '<svg height="10" width="15"><g><rect x= "5" y="0" width="10" height="10" fill ="' +
                    d.series[0].color + '"></rect></g></svg>' +
                    ' ' + name + ' : '  + count + '</div>';
                  }


                }
              }
            },
            title: {
              enable: true,
              text: 'OSHA Violations / Penalties'
            }
          };
        })
        .catch(function (err) {
          errorService.logError(err);
        });
    }]);
})();