/* global angular */
(function () {
  'use strict';
  angular.module('mvpUiApp')
  .controller('medicalCostsCtrl', ['$scope', '$rootScope', '_', '$', 'Risk360', 'riskAnalysisModalOptions', 'errorService', 'StateService',
    function ($scope, $rootScope, _, $, Risk360, riskAnalysisModalOptions, errorService, StateService) {
      $scope.modalOptions = riskAnalysisModalOptions;

      $scope.chartConfig = {
        xAxis: {
          type: 'categories',
          categories: []
        },
        yAxis: [],
        size: {
          width: 800,
          height: 400
        }
      };

      function categoryName(cost) {
        if (cost.granularity === 'city' && cost.state) {
          return cost.geography + ', ' + StateService.abbrevForState(cost.state);
        } else {
          return cost.geography;
        }
      }

      Risk360
      .getMedicalCostsData()
      .then(function (data) {
        $scope.medicalCosts = data;

        $scope.medicalCosts.localMedicalCosts = _.filter($scope.medicalCosts.localMedicalCosts, function (cost) {
          return angular.isObject(cost) &&
          angular.isDefined(cost.geography) &&
          angular.isDefined(cost.value);
        });

        if ($scope.medicalCosts.localMedicalCosts.length > 0) {
          $scope.allBelow = _.every($scope.medicalCosts.localMedicalCosts, function (cost) {
            return cost.value < $scope.medicalCosts.nationalMedicalCosts;
          });

          $scope.chartConfig = {
            chart: {
              type: 'column'
            },
            title: {
              text: 'Medical costs summary'
            },
            xAxis: {
              type: 'categories',
              categories: _.map($scope.medicalCosts.localMedicalCosts, function (cost) {
                return categoryName(cost);
              }),
              title: {
                text: 'Geography'
              }
            },
            yAxis: {
              title: {
                text: 'Cost'
              }
            },
            plotOptions: {
              column: {
                grouping: false,
                shadow: false,
                borderWidth: 0
              }
            },
            series: [{
              name: 'National Medical Costs',
              data: _.map($scope.medicalCosts.localMedicalCosts, function (cost) {
                return {
                  name: categoryName(cost),
                  y: $scope.medicalCosts.nationalMedicalCosts
                };
              }),
              tooltip: {
                valueDecimals: 2
              },
              color: 'rgba(253,207,120,0.8)',
              pointPadding: 0.3,
              pointPlacement: 0
            }, {
              name: 'Local Medical Costs',
              color: 'rgba(0,183,236,0.8)',
              data: _.map($scope.medicalCosts.localMedicalCosts, function (cost) {
                return {
                  name: categoryName(cost),
                  y: cost.value
                };
              }),
              tooltip: {
                valueDecimals: 2
              },
              pointPadding: 0.4,
              pointPlacement: 0
            }]
          };

          $('#medicalCostsChart').highcharts($scope.chartConfig);

        } else {
          $scope.medicalCosts = null;
        }
      })
      .catch(function (err) {
        $scope.medicalCosts = null;
        errorService.logError(err);
      });
    }]);
})();