/* global angular */
(function () {
  'use strict';

  angular
    .module('mvpUiApp')
    .directive('medicalCostsSummary', function () {
      return {
        templateUrl: 'app/underwriting/risk360/medicalCosts/medicalCostsTemplate.html',
        restrict: 'E',
        controller: 'medicalCostsCtrl',
        scope: {}
      };
    });
})();