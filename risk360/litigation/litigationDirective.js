/* global angular */
(function () {
  'use strict';

  angular
    .module('mvpUiApp')
    .directive('litigationSummary', function () {
      return {
        templateUrl: 'app/underwriting/risk360/litigation/litigationTemplate.html',
        restrict: 'E',
        controller: 'litigationCtrl',
        scope: {}
      };
    });
})();