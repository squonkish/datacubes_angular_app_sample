/* global angular, $ */
(function () {
  'use strict';

  angular
    .module('mvpUiApp')
    .controller('payrollCtrl', ['$scope', '$rootScope', '$filter', 'riskAnalysisModalOptions', 'Risk360', 'errorService', 'PayrollService', 'AssetService',
      function($scope, $rootScope, $filter, riskAnalysisModalOptions, Risk360, errorService, PayrollService, AssetService) {
        $scope.modalOptions = riskAnalysisModalOptions;

      // 'busy' module options
      //$scope.delay = 0;
      $scope.message = 'Loading';
      $scope.backdrop = false;

      $scope.payrollCollection = [];
      $scope.itemsByPage = 8;
      $scope.payrollDisplayed = [].concat($scope.payrollCollection);
      //PAYROLL MAP
      $scope.mapObject = {
        scope: 'usa',
        height: 400,
        options: {
          //legendHeight: 60 // optionally set the padding for the legend
        },
        geographyConfig: {
          highlightBorderColor: '#B9CDD3',
          highlightBorderWidth: 1,
          highlightFillColor: false,
          popupTemplate: function(geography, data) {
            return '<div class="hoverinfo" style="padding:10px;background:#F2F7F6">' +   data.state + '<br/> Regulation: ' + data.regulationRate + '<br/> Legal: ' + data.legalRate + ' ';
          },
          borderColor:'#ccc',
          borderWidth:1,
        },
        fills: {
          defaultFill: '#ffffff',
          'A': '#00ff45',
          'B': '#4bbc28',
          'C': '#bbbc28',
          'D': '#bc8228',
          'F': '#bc3e28'
        },
        data: {},
      };

      Risk360.getOverview($rootScope.getIntakeId(), $rootScope.getCompanyId())
        .then(function (overview) {
          $scope.company = overview.company;
        })
        .catch(function (err) {
          errorService.logError(err);
        });

      // GET RISK360 ACORD SUMMARY CARD
      AssetService.getAssetsSummary($rootScope.getCompanyId(), $rootScope.getStoryId())
        .success(function(data){
          if(data){
            // GET ASSET ID FROM ASSET SUMMARY API
            var acord = $filter('filter')(data, {type: 'acord'}, true);
            if (acord.length) {
              $scope.acordAssetId = acord[0].assetId;
            }

            // CALL ACORD ASSET
            if($scope.acordAssetId){
              PayrollService.getAcordAsset($rootScope.getIntakeId(), $scope.acordAssetId)
                .success(function(data){
                  $scope.rstreetPayrollSummary = data.nationalTotalPayrolls;
                })
                .error(function (err) {
                  errorService.logError(err);
                });
            }
          }
        })
        .error(function(error){
          errorService.logError(error);
        });

      // GET RISK360 RSTREET
      $scope.payrollBusy = $scope.payrollTablePromise = PayrollService.getPayroll($rootScope.getIntakeId(), $rootScope.getCompanyId())
        .success(function(data){
          $scope.payrollCollection = data.rstreet;
          $scope.rstreetStateCount = data.states.length;

          data.rstreet.forEach(function(state){{
              var newState = {};
              newState[state.state] = {
                fillKey: state.fillKey,
                state: state.state,
                legalRate: state.legalRate,
                regulationRate: state.regulationRate
              };
              $.extend($scope.mapObject.data, newState);
          }});
        });
  }]);
})();