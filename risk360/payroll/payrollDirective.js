/* global angular */
(function () {
  'use strict';

  angular
    .module('mvpUiApp')
    .directive('payrollSummary', function () {
      return {
        templateUrl: 'app/underwriting/risk360/payroll/payrollTemplate.html',
        restrict: 'E',
        controller: 'payrollCtrl',
        scope: {}
      };
    });
})();