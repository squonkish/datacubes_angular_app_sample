/* global angular */
(function () {
  'use strict';
  angular
    .module('mvpUiApp')
    .factory('PayrollService', ['$http', 'ENV', function ($http, ENV) {
      return {
        getAcordAsset: getAcordAsset,
        getPayroll: getPayroll
      };

      function getAcordAsset(intakeId, acordAssetId) {
        return $http.get(ENV.BASE_URL + ENV.API_INTAKE + intakeId + ENV.API_ACORD + acordAssetId +'/payroll');
      }

      function getPayroll(intakeId, companyId) {
        return $http.get(ENV.BASE_URL + ENV.API_INTAKE + intakeId + '/risk360/' + companyId + '/payroll');
      }
    }]);
})();