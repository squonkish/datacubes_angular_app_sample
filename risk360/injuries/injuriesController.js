/* global angular */
(function () {
  'use strict';

  angular
    .module('mvpUiApp')
    .controller('injuriesCtrl', ['$scope', '$rootScope', '_', '$', 'Risk360', 'errorService', 'riskAnalysisModalOptions',
      function($scope, $rootScope, _, $, Risk360, errorService, riskAnalysisModalOptions) {
        $scope.modalOptions = riskAnalysisModalOptions;

        function generateFullSeriesByGraphKey(categories, dataset) {
          return _.map(categories, function (quarterGraphKey) {
            return _.result(_.find(dataset, ['quarterGraphKey', quarterGraphKey]), 'count') || 0;
          });
        }

      Risk360.getOverview($rootScope.getIntakeId(), $rootScope.getCompanyId())
        .then(function (data) {
          $scope.overview = data.overview;
        })
        .catch(function (err) {
          errorService.logError(err);
        });

      Risk360
        .getInjuriesData()
        .then(function (data) {
          $scope.data = data;

          data.osha_fatalities = _.sortBy(data.osha_fatalities, 'quarterSortKey');
          data.osha_non_fatalities = _.sortBy(data.osha_non_fatalities, 'quarterSortKey');

          var allCategories = _.uniq(_.map(_.sortBy(_.concat(data.osha_fatalities, data.osha_non_fatalities), 'quarterSortKey'), 'quarterGraphKey'));
          var fatalSeries = generateFullSeriesByGraphKey(allCategories, data.osha_fatalities);
          var nonFatalSeries = generateFullSeriesByGraphKey(allCategories, data.osha_non_fatalities);

          var chartOptions = {
            chart: {
              type: 'column'
            },
            title: 'Workplace Injuries / Fatalities',
            xAxis: {
              categories: allCategories
            },
            series: [],
            colors: ['#00B7EC', '#FDCF78']
          };

          if (data.osha_fatalities.length > 0) {
            chartOptions.series.push({
              name: 'Fatal',
              data: fatalSeries
            });
          }

          if (data.osha_non_fatalities.length > 0) {
            chartOptions.series.push({
              name: 'Non-Fatal',
              data: nonFatalSeries
            });
          }

          $('#fatalitiesChart').highcharts(chartOptions);
        })
        .catch(function (err) {
          errorService.logError(err);
        });

    }]);
})();