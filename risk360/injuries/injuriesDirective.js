/* global angular */
(function () {
  'use strict';

  angular
    .module('mvpUiApp')
    .directive('injuriesSummary', function () {
      return {
        templateUrl: 'app/underwriting/risk360/injuries/injuriesTemplate.html',
        restrict: 'E',
        controller: 'injuriesCtrl',
        scope: {}
      };
    });
})();