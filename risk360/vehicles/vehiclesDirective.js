(function () {
  'use strict';

  angular
    .module('mvpUiApp')
    .directive('vehiclesSummary', function () {
      return {
        templateUrl: 'app/underwriting/risk360/vehicles/vehiclesTemplate.html',
        restrict: 'E',
        controller: 'vehiclesCtrl',
        scope: {}
      };
    });
}());