/* global angular */
(function () {
  'use strict';
  angular.module('mvpUiApp')
  .controller('vehiclesCtrl', ['$scope', 'Risk360', 'errorService',
    function ($scope, Risk360, errorService) {
      $scope.showVehicles = function showVehicles() {
        return $scope.totalDrivers && $scope.averageMilesDriven && $scope.nationalAverageMilesDriven;
      };

      $scope.vehiclesComparison = function vehiclesComparison() {
        if ($scope.averageMilesDriven > $scope.nationalAverageMilesDriven) {
          return 'exceed';
        } else if ($scope.averageMilesDriven < $scope.nationalAverageMilesDriven) {
          return 'lower than';
        } else {
          return 'equal to';
        }
      };

      Risk360
        .getVehiclesData()
        .then(function (data) {
          angular.extend($scope, data);
        })
        .catch(function (err) {
          errorService.logError(err);
        });
    }]);
}());