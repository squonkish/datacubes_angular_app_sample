/* global angular */
(function () {
  'use strict';

  angular.module('mvpUiApp')
    .controller('risk360Ctrl', ['$scope', '$rootScope', 'Risk360', 'errorService', 'modalService',
      function($scope, $rootScope, Risk360, errorService, modalService) {
        $scope.overviewData = null;   // Reset to initial state so it's not misleading when switching.

        $scope.companyId = $rootScope.getCompanyId();
        $scope.intakeId = $rootScope.getIntakeId();

        Risk360.getOverview($scope.intakeId, $scope.companyId)
          .then(function (data) {
              $scope.overviewData = data;
          })
          .catch(function (err) {
              errorService.logError(err);
          });

        $scope.message = 'Loading';
        $scope.backdrop = true;

        $scope.isActive = false;
        $scope.activeButton = function() {
          $scope.isActive = !$scope.isActive;
        };

        $scope.socialBusy = Risk360.getOverview($scope.intakeId, $scope.companyId);

        $scope.triggerShareModal = function () {
          modalService.show({
            templateUrl: 'app/underwriting/risk360/underwritingRisk360ExcelView.html',
            controller: 'risk360ExcelCtrl'
          });
        };
    }])
    .controller('risk360ExcelCtrl', ['$http', '$scope', '$rootScope', '$timeout', 'ENV', 'errorService', '$uibModalInstance',
      function ($http, $scope, $rootScope, $timeout, ENV, errorService, $uibModalInstance) {
        $scope.busy = false;
        $scope.done = false;
        $scope.error = false;

        $scope.buttonText = function buttonText() {
         return $scope.busy ? 'Please wait...' : 'Generate Excel File';
        };

        $scope.generateFile = function generateFile() {
          $scope.error = false;
          $scope.done = false;
          $scope.busy = true;

          $http.get(ENV.BASE_URL + ENV.API_INTAKE + $rootScope.getIntakeId() + '/risk360/' + $rootScope.getCompanyId() + '/export')
            .then(function (response) {
              // response.data.data.url = 'http://www.sample-videos.com/xls/Sample-Spreadsheet-1000-rows.xls';
              if (response && response.data && response.data.data && response.data.data.url) {
                window.open(response.data.data.url, '_self');
                $timeout($uibModalInstance.close, 1000);
              } else {
                if (response && response.data && response.data.message) {
                  errorService.logError(JSON.stringify(response.data.message));
                } else {
                  errorService.logError('No URL to download!');
                }
                $scope.error = true;
              }
            })
            .catch(function (err) {
              $scope.error = true;
              errorService.logError(err);
            })
            .finally(function () {
              $scope.done = true;
              $scope.busy = false;
            });
        };
      }
    ]);
})();
