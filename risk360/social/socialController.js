/* global angular */
(function () {
  'use strict';
  angular.module('mvpUiApp')
    .controller('socialCtrl', ['$scope', '$rootScope', 'Risk360', 'riskAnalysisModalOptions', 'errorService',
      function ($scope, $rootScope, Risk360, riskAnalysisModalOptions, errorService) {
      $scope.modalOptions = riskAnalysisModalOptions;

      Risk360
        .getOverview($rootScope.getIntakeId(), $rootScope.getCompanyId())
        .then(function (data) {
          $scope.overviewData = data;
        })
        .catch(function (err) {
          $scope.overviewData = null;
          errorService.logError(err);
        });
    }]);
})();