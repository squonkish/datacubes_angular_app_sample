/* global angular */
(function () {
  'use strict';

  angular
    .module('mvpUiApp')
    .directive('socialSummary', function () {
      return {
        templateUrl: 'app/underwriting/risk360/social/socialTemplate.html',
        restrict: 'E',
        controller: 'socialCtrl',
        scope: {}
      };
    });
})();