/* global angular, d3 */
(function () {
  'use strict';

  angular.module('mvpUiApp')
  .controller('lossrunCtrl', ['$rootScope', '$scope', '$filter', 'AssetService', 'LossRunService', 'errorService', 'riskAnalysisModalOptions',
    function ($rootScope, $scope, $filter, AssetService, LossRunService, errorService, riskAnalysisModalOptions) {
      $scope.modalOptions = riskAnalysisModalOptions;

      AssetService.getAssetsSummary($rootScope.getCompanyId(), $rootScope.getStoryId())
      .success(function(data){
        if(data){
            // GET LOSS RUN ASSET ID FROM ASSET SUMMARY API
            var lossrun = $filter('filter')(data, {type: 'loss-run'}, true);
            if (lossrun.length) {
              $scope.lossrunAssetId = lossrun[0].assetId;
            }
            // CALL LOSS RUN ASSET
            if($scope.lossrunAssetId){
              AssetService.getLossRunAsset($rootScope.getIntakeId(), $scope.lossrunAssetId)
              .then(function (res) {
                $scope.severity = res.severity;
              })
              .catch(function (err) {
                errorService.logError(err);
              });
            }
          }
        })
      .catch(function (err) {
        errorService.logError(err);
      });

      LossRunService.getLossRun($rootScope.getIntakeId(), $rootScope.getCompanyId())
      .then(function (data) {
        if (data[0].values.length === 0 && data[1].values.length === 0) {
          throw('No values to display for loss run chart!');
        }

        $scope.data = data;

        var maxHeight1 = d3.max($scope.data[0].values, function (d) {
          return parseFloat(d[1]);
        });

        var maxHeight2 = d3.max($scope.data[1].values, function (d) {
         return parseFloat(d[1]);
        });

        var max1 = maxHeight1 + 50;
        var max2 = maxHeight2 + 50;

        $scope.options = {

          chart: {
            type: 'linePlusBarChart',
            height: 300,
            margin: {
              top: 30,
              right: 75,
              bottom: 50,
              left: 75
            },
            zoom:11,
            interpolate:'cardinal',
            focusEnable: false,
            bars: {
              forceY: [0, max1]
            },
            bars2: {
              forceY: [0, max1]
            },
            lines: {
              forceY: [0, max2]
            },
            lines2: {
              forceY: [0, max2]
            },
            color: ['#8064B6', '#00B7EC'],
            x: function(d, i) {return i;},
            y: function(d) {return d[1]; },
            xAxis: {
              tickFormat: function(d) {
                var year = $scope.data[0].values[d] && $scope.data[0].values[d][0]|| 0;
                var quarter = $scope.data[0].values[d] && $scope.data[0].values[d][2]|| 0;
                if ( quarter === 0 ) {
                  quarter = 1;
                }
                var dx = "Q" + quarter;
                dx += '-';
                dx += year;
                return dx;
              },
              showMaxMin: true
            },
            x2Axis: {
              tickFormat: function(d) {
                var dx = $scope.data[0].values[d] && $scope.data[0].values[d][0]|| 0;
                return dx;
              },
              showMaxMin: true
            },
            y1Axis: {
              axisLabel: '',
              tickFormat: function(d){
                return '$' + d3.format(',f')(d);
              },

            },
            y2Axis: {
              axisLabel: '',
              tickFormat: function(d) {
                return '$' + d3.format(',.2f')(d);
              },
            },
            legendLeftAxisHint: '',
            legendRightAxisHint: '',
            tooltips:true,
            tooltip: {
              contentGenerator: function (d) {
                var quarter, cost, name;
                if( typeof d.point && d.point ) {
                  quarter = 'Q' + d.point[2];
                  quarter += '-';
                  quarter += d.point[0];
                  cost = d.point[1];
                  name = 'Average Loss Costs';
                }
                else if( typeof d.data && d.data ) {
                  quarter = 'Q' + d.data[2];
                  quarter += '-';
                  quarter += d.data[0];
                  cost = d.data[1];
                  name = 'Incidents';
                }

                return '<p>' + quarter + '</p>' +
                '<div>' + '<svg height="10" width="15"><g><rect x= "5" y="0" width="10" height="10" fill ="' +
                d.series[0].color + '"></rect></g></svg>' +
                ' ' + name + ' ' + $filter('currency')(cost,'$',2) + '</div>';
              }
            }
          },
          title: {
            enable: true,
            text: 'Average Loss Costs'
          }
        };
      });
  }]);
})();