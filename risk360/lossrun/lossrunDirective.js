/* global angular */
(function () {
  'use strict';

  angular
    .module('mvpUiApp')
    .directive('lossrunSummary', function () {
      return {
        templateUrl: 'app/underwriting/risk360/lossrun/lossrunTemplate.html',
        restrict: 'E',
        controller: 'lossrunCtrl',
        scope: {}
      };
    });
})();