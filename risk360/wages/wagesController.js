/* global angular, $ */
(function () {
  'use strict';

  angular
    .module('mvpUiApp')
    .controller('wagesCtrl', ['$scope', '$sce', '$rootScope', '$http', 'ENV', '$filter', 'Risk360', 'riskAnalysisModalOptions',
    function($scope, $sce, $rootScope, $http, ENV, $filter, Risk360, riskAnalysisModalOptions) {
      $scope.modalOptions = riskAnalysisModalOptions;

    $scope.message = 'Loading';
    $scope.backdrop = false;

    $scope.payrollCollection = [];
    $scope.itemsByPage = 8;

    $scope.hasAcordData = function hasAcordData() {
      if ($scope.wagesData && $scope.wagesData.length > 0) {
        return angular.isDefined($scope.wagesData[0].payroll) &&
                angular.isDefined($scope.wagesData[0].nationalAverage) &&
                angular.isDefined($scope.wagesData[0].stateAverage) &&
                angular.isDefined($scope.wagesData[0].employees);
      } else {
        return false;
      }
    };

    //WAGES MAP
    $scope.mapObject = {
      scope: 'usa',
      height: 450,
      options: {
        //legendHeight: 60 // optionally set the padding for the legend
      },
      geographyConfig: {
        highlightBorderColor: '#B9CDD3',
        highlightBorderWidth: 1,
        highlightFillColor: false,
        popupTemplate: function(geography, data) {
          if ($scope.hasAcordData()) {
            return '<div class="hoverinfo" style="padding:10px;background:#F2F7F6"><strong>' +
            data.state +  '</strong><br/> Total Employees: <strong>' +
            data.employees + '</strong> ' + '<br/> Company Payroll: <strong>' +
            $filter('currency')(data.payroll,'$', 0) + '</strong><br/> State Average: <strong>' +
            $filter('currency')(data.stateAverage,'$', 0) + '</strong><br/> National Average: <strong>' +
            $filter('currency')(data.nationalAverage,'$', 0) + '</strong>';
          } else {
            return '<div class="hoverinfo" style="padding:10px;background:#F2F7F6"><strong>' +
              data.state + '</strong><br/>' +
              'National Average: <strong>' + $filter('currency')(data.nationalAverage, '$', 0) + '</strong><br/>' +
              'FIPS Code: <strong>' + data.fipsCode + '</strong><br/>' +
              'NAICS Code: <strong>' + data.naics + '</strong>';
          }
        },
        borderColor:'#ccc',
        borderWidth:1,
      },
      fills: {
        defaultFill: '#ffffff',
        'above': '#bc3e28',
        'below': '#00ff45',
        'equal': '#DDD'
      },
      data: {},
    };

    Risk360
      .getWagesData()
      .then(function (wages) {
        $scope.wagesData = wages;

        if ($scope.hasAcordData()) {
          if (_.every(wages, function (wage) {
            return wage.stateAverage < wage.nationalAverage;
          })) {
            $scope.summaryDirection = 'below';
          } else if (_.every(wagesArr, function (wage) {
            return wage.stateAverage > wage.nationalAverage;
          })) {
            $scope.summaryDirection = 'above';
          } else {
            $scope.summaryDirection = 'mixed';
          }
        }

        $scope.wagesData.forEach(function(state) {
          var newState = {};
          newState[state.state] = {
            fillKey: fillKeyFunction(state),
            state: state.state,
            employees: state.employees,
            payroll: state.payroll,
            stateAverage: state.stateAverage,
            nationalAverage: state.nationalAverage,
            fipsCode: state.fipsCode,
            naics: state.naics
          };
          $.extend($scope.mapObject.data, newState);
        });
      });

    function fillKeyFunction(state) {
      if (state.stateAverage < state.nationalAverage) {
        return 'below';
      } else if (state.stateAverage > state.nationalAverage) {
        return 'above';
      } else {
        return 'equal';
      }
    }
  }]);
})();