/* global angular */
(function () {
  'use strict';

  angular
    .module('mvpUiApp')
    .directive('wagesSummary', function () {
      return {
        templateUrl: 'app/underwriting/risk360/wages/wagesTemplate.html',
        restrict: 'E',
        controller: 'wagesCtrl',
        scope: {}
      };
    });
})();