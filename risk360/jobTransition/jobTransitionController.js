/* global angular */
(function () {
  'use strict';

  angular
    .module('mvpUiApp')
    .controller('jobTransitionCtrl', ['$scope', '$filter', '_', 'Risk360', 'riskAnalysisModalOptions', 'errorService',
      function($scope, $filter, _, Risk360, riskAnalysisModalOptions, errorService) {
      $scope.modalOptions = riskAnalysisModalOptions;

      var summaryData;

      $scope.hasJobTransitionData = function hasJobTransitionData() {
        return $scope.data;
      };

      Risk360.getJobTransitionData()
        .then(function (response) {
          $scope.data = response;
        })
        .catch(function (err) {
          $scope.data = null;
          errorService.logError(err);
        });

      Risk360.getJobTransitionSummaryData()
        .then(function (response) {
          summaryData = response;

          if (angular.isArray(summaryData.regionalAverage) && summaryData.regionalAverage.length > 0) {
            $scope.dataType = 'regional';
            $scope.jtRatio = _.meanBy(summaryData.regionalAverage, 'average') / summaryData.overAllNationalAverage;
          } else {
            $scope.dataType = 'industry';
            $scope.jtRatio = summaryData.nationalIndustryAverage / summaryData.overAllNationalAverage;
          }
        })
        .catch(function (err) {
          summaryData = null;
          errorService.logError(err);
        });

      $scope.options = {
        chart: {
          type: 'lineChart',
          height: 250,
          margin : {
            top: 20,
            right: 20,
            bottom: 40,
            left: 55
          },
          tooltip: {
            keyFormatter: function(d) {
              return d;
            }
          },
          interpolate: 'cardinal',
          lines: {forceY:[0]},
          color: ['#7777ff', '#3b3b7f', '#FFB732', '#e55c00'],
          x: function(d){ return new Date(d.label); },
          y: function(d){ return d.value; },
          xAxis: {
            tickFormat: function(d){
              return $filter('date')(d, 'MMM yyyy');
            }
          },
          yAxis: {
            axisLabel: '',
            tickFormat: function(d){
              return d;
            },
            axisLabelDistance: 0
          },
            callback: function(){}
        },
        title: {
          enable: true,
          text: 'Job Transitions'
        },
        subtitle: {
            enable: false,
          css: {
            'text-align': 'center',
            'margin': '10px 13px 0px 7px'
          }
        },
        caption: {
            enable:false,
            css: {
              'text-align': 'justify',
              'margin': '10px 13px 0px 7px'
            }
          }
        };
      }]);
})();