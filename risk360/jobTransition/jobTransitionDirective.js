/* global angular */
(function () {
  'use strict';

  angular
    .module('mvpUiApp')
    .directive('jobTransitionSummary', function () {
      return {
        templateUrl: 'app/underwriting/risk360/jobTransition/jobTransitionTemplate.html',
        restrict: 'E',
        controller: 'jobTransitionCtrl',
        scope: {}
      };
    });
})();