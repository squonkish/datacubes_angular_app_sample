/* global angular */

(function () {
  'use strict';
  angular.module('mvpUiApp')
    .factory('Risk360', ['$http', '$q', '$rootScope', 'ENV', '_', 'StateService', 'errorService',
      function ($http, $q, $rootScope, ENV, _, StateService, errorService) {
      var localCache = [];
      var overviewPromise;

      return {
        getOverview: getOverview,
        getJobTransitionData: getJobTransitionData,
        getJobTransitionSummaryData: getJobTransitionSummaryData,
        getInjuriesData: getInjuriesData,
        getViolationsData: getViolationsData,
        getBenchmarkingData: getBenchmarkingData,
        getWagesData: getWagesData,
        getMedicalCostsData: getMedicalCostsData,
        getVehiclesData: getVehiclesData,
        clearOverviewCache: clearOverviewCache
      };

      function clearOverviewCache() {
        errorService.logDebug('Overview cache cleared.');
        localCache = [];
      }

      function getOverview(intakeId, companyId) {
        if (ENV.STATIC_OVERVIEW) {
          return $q.resolve({"company":{"companyId":"2dab1489-0b32-406f-a475-d653856e97c7","name":"Phillips Manufacturing Co.","dunsNum":"05-331-1197","description":"Metal Window and Door Manufacturing","naics":"332321","establishDate":"2009","numOfEmployee":228,"siteEmployee":118,"annualSales":50946644,"streetAddr1":"4949 S 30TH ST","streetAddr2":"","city":"Omaha","state":"Nebraska","country":"USA","createTimestamp":1464802606000,"updatedTimestamp":1464804331000,"companyKey":null,"status":null},"overview":{"JT":[{"category":"overAllNationalSeparationRates","value":{"monthlyData":[{"value":3.9,"month":"January"},{"value":3.0,"month":"February"},{"value":3.1,"month":"March"}],"year":"2016"}},{"category":"overAllNationalSeparationRates","value":{"monthlyData":[{"value":3.9,"month":"January"},{"value":2.7,"month":"February"},{"value":3.1,"month":"March"},{"value":3.4,"month":"April"},{"value":3.4,"month":"May"},{"value":3.6,"month":"June"},{"value":3.6,"month":"July"},{"value":4.2,"month":"August"},{"value":3.7,"month":"September"},{"value":3.5,"month":"October"},{"value":3.0,"month":"November"},{"value":3.3,"month":"December"}],"year":"2015"}},{"category":"MW","value":{"monthlyData":[{"value":3.9,"month":"January"},{"value":3.0,"month":"February"},{"value":3.3,"month":"March"}],"year":"2016"}},{"category":"MW","value":{"monthlyData":[{"value":3.7,"month":"January"},{"value":2.6,"month":"February"},{"value":3.0,"month":"March"},{"value":3.4,"month":"April"},{"value":3.3,"month":"May"},{"value":3.4,"month":"June"},{"value":3.6,"month":"July"},{"value":4.3,"month":"August"},{"value":3.6,"month":"September"},{"value":3.5,"month":"October"},{"value":3.2,"month":"November"},{"value":3.4,"month":"December"}],"year":"2015"}},{"category":"nationalIndustrySeparationRates","value":{"monthlyData":[{"value":2.1,"month":"January"},{"value":2.2,"month":"February"},{"value":2.1,"month":"March"}],"year":"2016"}},{"category":"nationalIndustrySeparationRates","value":{"monthlyData":[{"value":2.0,"month":"January"},{"value":1.7,"month":"February"},{"value":1.8,"month":"March"},{"value":2.0,"month":"April"},{"value":1.8,"month":"May"},{"value":2.1,"month":"June"},{"value":2.3,"month":"July"},{"value":2.7,"month":"August"},{"value":2.3,"month":"September"},{"value":2.0,"month":"October"},{"value":1.7,"month":"November"},{"value":1.8,"month":"December"}],"year":"2015"}},{"category":"MW","value":{"monthlyData":[{"value":3.7,"month":"January"},{"value":2.6,"month":"February"},{"value":3.0,"month":"March"},{"value":3.4,"month":"April"},{"value":3.3,"month":"May"},{"value":3.4,"month":"June"},{"value":3.6,"month":"July"},{"value":4.3,"month":"August"},{"value":3.6,"month":"September"},{"value":3.5,"month":"October"},{"value":3.2,"month":"November"},{"value":3.4,"month":"December"}],"year":"2015"}},{"category":"MW","value":{"monthlyData":[{"value":3.9,"month":"January"},{"value":3.0,"month":"February"},{"value":3.3,"month":"March"}],"year":"2016"}}],"TCR_INDUSTRY":[{"value":"4.4","year":"2014"}],"DAFW_COMPANY":{"value":"179824.56","year":"2014"},"osha_violations_qtr":null,"JT_STATEMENT":{"nationalIndustryAverage":1.9416666666666667,"regionalAverage":[{"region":"MW","average":3.2083333333333335},{"region":"MW","average":3.2083333333333335}],"overAllNationalAverage":3.2000000000000006},"litigation_findings":null,"wages":[{"NationalAverage":"53338","companyAverage":"35883.8444444444","state":"Ohio","payroll":"3229546.00","employees":"90"},{"NationalAverage":"53338","companyAverage":"61097.7894736842","state":"Nebraska","payroll":"2152182.00","employees":"19"},{"NationalAverage":"53338","companyAverage":"43247.3333333330","state":"Arizona","payroll":"0.00","employees":"1"},{"NationalAverage":"53338","companyAverage":"9837.7796610169","state":"Nebraska","payroll":"2152182.00","employees":"118"}],"NON_TOTAL":[{"value":"109.9","year":"2011"},{"value":"149.1","year":"2012"},{"value":"110.4","year":"2013"},{"value":"118.0","year":"2014"}],"non_fatalities_3yravg":null,"litigations_desc":null,"TCR_COMPANY":{"value":"0.065789476","year":"2014"},"osha_non_fatalities":null,"DAFW_INDUSTRY":[{"value":"109.9","year":"2011"},{"value":"149.1","year":"2012"},{"value":"110.4","year":"2013"},{"value":"118.0","year":"2014"}],"fatalities_3yravg":null,"osha_accidents_total":null,"glassdoor":{"featuredReview_jobTitle":"Employee","centi_score":"Good Score","ceo_name":"George J Kubat","workLifeBalanceRating":"3.8","recommendToFriendRating":"1.0","squareLogo":"","industryName":"Metal & Mineral Manufacturing","seniorLeadershipRating":"3.5","id":"764466","featuredReview_pros":"good people good atmosphere flexiable","ceo_numberOfRatings":"1","featuredReview_attributionURL":"http://www.glassdoor.com/Reviews/Employee-Review-Phillips-Manufacturing-RVW9436630.htm","featuredReview_reviewDateTime":"2016-02-03 06:51:00.687","featuredReview_cons":"a margin driven company in a volume driven industry","ratingDescription":"Satisfied","featuredReview_location":"Girard, OH","careerOpportunitiesRating":"3.7","sectorName":"Manufacturing","ceo_title":"President & CEO","website":"www.phillipsmfg.com","ceo_pctDisapprove":"0","featuredReview_id":"9436630","featuredReview_currentJob":"False","featuredReview_overall":"4","compensationAndBenefitsRating":"3.5","exactMatch":"True","cultureAndValuesRating":"3.6","isEEP":"False","featuredReview_headline":"sales","message":"FOUND_ENTRY","overallRating":"4","industryId":"200074","ceo_pctApprove":"100","name":"Phillips Manufacturing","featuredReview_jobTitleFromDb":"","industry":"Metal & Mineral Manufacturing","numberOfRatings":"4","sectorId":"10015","featuredReview_overallNumeric":"4"},"osha_fatalities":null,"medical_costs":{"nationalMedicalCosts":1.53,"localMedicalCosts":[{"state":"AK","geography":"AK","granularity":"state","value":1.48},{"state":"IL","geography":"Tazewell","granularity":"county","value":1.60},{"state":"NY","geography":"Albany","granularity":"city","value":2.12}]}}});
        } else {
          if (overviewPromise) {
            return overviewPromise;
          }

          var hash = intakeId + '-' + companyId;

          if (angular.isDefined(localCache[hash])) {
            return $q.resolve(localCache[hash]);
          } else {
            overviewPromise = $q(function (resolve, reject) {
              $http.get([ENV.BASE_URL, ENV.API_INTAKE, intakeId, '/risk360/', companyId, '/overview'].join(''))
                .then(function (response) {
                  localCache[hash] = response.data;
                  $rootScope.overviewData = response.data;
                  resolve(response.data);
                })
                .catch(function (err) {
                  reject(err);
                })
                .finally(function () {
                  overviewPromise = null;
                });
              });
          }

          return overviewPromise;
        }
      }

      function getJobTransitionData() {
        return $q(function (resolve, reject) {
          getOverview($rootScope.getIntakeId(), $rootScope.getCompanyId())
            .then(function (data) {
              if (Object.getOwnPropertyNames(data).length <= 0)  {
                reject('No data!');
                return;
              }

              if (typeof data.overview === "undefined" || data.overview === null) {
                reject('No overview data!');
                return;
              }

              if (!data.overview.JT) {
                reject('No Job Transition Data!');
                return;
              }

              var valueArr = [];

              var jtCats = _.groupBy(data.overview.JT, 'category');

              _.each(jtCats, function (categoryDetail) {
                var categoryData = {
                  key: categoryDetail[0].category,
                  values: []
                };

                // For each category subValue
                _.each(categoryDetail, function (annualData) {
                  var year = annualData.value.year;

                  _.each(annualData.value.monthlyData, function (monthlyDetail) {
                    categoryData.values.push({
                      label: monthlyDetail.month + ' ' + year,
                      value: monthlyDetail.value
                    });
                  });
                });

                categoryData.values = _.uniqBy(categoryData.values, 'label');
                categoryData.values = _.sortBy(categoryData.values, function (chartFriendlyData) {
                  return new Date(chartFriendlyData.label);
                });

                valueArr.push(categoryData);
              });

              resolve(valueArr);
            })
            .catch(function (err) {
              reject(err);
            });
        });
      }

      function getJobTransitionSummaryData() {
        return $q(function (resolve, reject) {
          getOverview($rootScope.getIntakeId(), $rootScope.getCompanyId())
            .then(function (data) {
              if (!data || !data.overview || !data.overview.JT_STATEMENT) {
                reject('No JT_STATEMENT information in overview!');
                return;
              }

              resolve(data.overview.JT_STATEMENT);
            })
            .catch(function (err) {
              reject(err);
            });
        });
      }

      function hasAcordFields(wageObj) {
        return angular.isDefined(wageObj.employees) &&
          angular.isDefined(wageObj.payroll) &&
          angular.isDefined(wageObj.NationalAverage);
      }

      function getWagesData() {
        return $q(function (resolve, reject) {
          getOverview($rootScope.getIntakeId(), $rootScope.getCompanyId())
            .then(function (data) {
              if (Object.getOwnPropertyNames(data).length <= 0)  {
                reject('No data!');
                return;
              }

              if (typeof data.overview === "undefined" || data.overview === null) {
                reject('No overview data!');
                return;
              }

              if (!data.overview.wages || !angular.isArray(data.overview.wages)) {
                reject('No wages data!');
                return;
              }

              var wagesArr = [];

              if (data.overview.wages.length > 0 && hasAcordFields(data.overview.wages[0])) {
                var result = [];
                var nationalAverage = data.overview.wages[0].NationalAverage;

                // Aggregate the different company branches into a single object for employees
                _.each(
                  _.groupBy(data.overview.wages, 'state'), function (item) {
                    var employees = _.reduce(item, function (employees, value) {
                      employees = _.toNumber(value.employees) + employees;
                      return employees;
                    }, 0);

                    var payroll = _.reduce(item, function (payroll, value) {
                      payroll = _.toNumber(value.payroll) + payroll;
                      return payroll;
                    }, 0);

                    var resultObj = {
                      employees: employees,
                      payroll: payroll,
                      state: item[0].state,
                      stateAverage: item[0].companyTotal / employees,
                      companyTotal: item[0].companyTotal
                    };

                    result.push(resultObj);
                  }
                );

                wagesArr = _.map(result, function (wageObj) {
                  return {
                    nationalAverage: nationalAverage,
                    stateAverage: wageObj.stateAverage,
                    employees: wageObj.employees,
                    payroll: wageObj.companyTotal,
                    state: StateService.abbrevForState(wageObj.state)
                  };
                });
              } else {
                wagesArr = _.map(data.overview.wages, function (wageObj) {
                  return {
                    nationalAverage: wageObj.avgAnnualPay,
                    fipsCode: wageObj.areaFips,
                    naics: wageObj.naics,
                    state: StateService.abbrevForState(wageObj.state)
                  };
                });
              }

              resolve(wagesArr);
            })
            .catch(function (err) {
              reject(err);
            });
        });
      }

      function getInjuriesData() {
        function getQuarter(str) {
          var strQtr = ['FIRST', 'SECOND', 'THIRD', 'FOURTH'];
          var qtr = 0;
          for (var i = 0; i < strQtr.length; i ++) {
              if(strQtr[i] == str) {
                  qtr = i + 1;
                  return qtr;
              }
          }
          if (qtr === 0) {
            return str;
          }
        }

        return $q(function (resolve, reject) {
          getOverview($rootScope.getIntakeId(), $rootScope.getCompanyId())
            .then(function (data) {
              if (Object.getOwnPropertyNames(data).length <= 0)  {
                reject('No data!');
                return;
              }

              if (typeof data.overview === "undefined" || data.overview === null) {
                reject('No overview data!');
                return;
              }

              function mapOshaData(dataset) {
                return _.map(dataset, function (item) {
                  return {
                    count: item.count,
                    quarterSortKey: new Date(getQuarter(item.qtr) + '/1' + item.yr),
                    quarterGraphKey: 'Q' + getQuarter(item.qtr) + '-' + item.yr
                  };
                });
              }

              // Use this data to test worst-case scenarios!

              // resolve({
              //   osha_non_fatalities: [{
              //     count: 25,
              //     quarterSortKey: new Date('9/1/2015'),
              //     quarterGraphKey: 'Q3-2015'
              //   }, {
              //     count: 52,
              //     quarterSortKey: new Date('1/1/2015'),
              //     quarterGraphKey: 'Q1-2015'
              //   }],
              //   osha_fatalities: [{
              //     count: 4,
              //     quarterSortKey: new Date('9/1/2015'),
              //     quarterGraphKey: 'Q3-2015'
              //   }, {
              //     count: 12,
              //     quarterSortKey: new Date('4/1/2015'),
              //     quarterGraphKey: 'Q2-2015'
              //   }],
              //   fatalities_3yravg: 10,
              //   osha_accidents_total: 1000
              // });

              resolve({
                osha_non_fatalities: mapOshaData(data.overview.osha_non_fatalities),
                osha_fatalities: mapOshaData(data.overview.osha_fatalities),
                fatalities_3yravg: data.overview.fatalities_3yravg,
                osha_accidents_total: data.overview.osha_accidents_total
              });
            })
            .catch(function (err) {
              reject(err);
            });
        });
      }

      function getViolationsData() {
        var oshaViolationsData = [];

        function sortData(data) {
          data.sort(function(a, b){
            return parseInt(a[0]) - parseInt(b[0]);
          });

          data.sort(function(a, b){
            return parseInt(a[1]) - parseInt(b[1]);
          });

          return data;
        }

        function inArray(val, arr) {
          for(var i = 0; i < arr.length; i ++) {
            if(val[0] == arr[i][0] && val[1] == arr[i][1]) {
              return true;
            }
          }

          return false;
        }

        function getQuarter(string) {
          var strQtr = ['FIRST', 'SECOND', 'THIRD', 'FOURTH'];
          var qtr = 0;
          for (var i = 0; i < strQtr.length; i ++) {
            if(strQtr[i] == string) {
              qtr = i + 1;
              return qtr;
            }
          }
          if (qtr === 0) return string;
        }

        return $q(function (resolve, reject) {
          getOverview($rootScope.getIntakeId(), $rootScope.getCompanyId())
            .then(function (data) {
              if( Object.getOwnPropertyNames(data).length <= 0 || !data ||
                data.overview === null) {
                return oshaViolationsData;
              } else if( !data.hasOwnProperty('overview') ||
                !data.overview.hasOwnProperty('osha_violations_qtr') ) {
                return oshaViolationsData;
              } else {
                var violations = [],
                    penalties = [],
                    indAvgFatals = [],
                    indAvgNonFatals = [],
                    quarters = [],
                    fatals,
                    key,
                    fatalsKey = [],
                    fatalsVal = [];

                if(data.overview.osha_violations_qtr !== null) {

                  angular.forEach(data.overview.osha_violations_qtr, function(item) {
                    item.qtr = getQuarter(item.qtr);
                    violations.push([item.qtr, item.yr, item.count]);
                    penalties.push([item.qtr, item.yr, item.penalty]);
                    quarters.push([item.qtr, item.yr]);
                  });

                  violations = sortData(violations);
                  oshaViolationsData.push({'key' : 'Violation', 'values' : violations, "bar": true});

                  penalties = sortData(penalties);
                  oshaViolationsData.push({'key' : 'Penalty', 'values' : penalties});

                  quarters = sortData(quarters);

                }

                if (data.overview.hasOwnProperty('non_total') && data.overview.non_total !== null) {
                  fatals = data.overview.non_total;
                  fatalsKey = [];
                  fatalsVal = [];

                  for (key in fatals) {
                    fatalsKey.push(key);
                    fatalsVal.push(fatals[key]);
                  }

                  if(fatals !== null) {
                    angular.forEach(quarters, function(item) {
                      var i = fatalsKey.indexOf( item[1].toString() );
                      if( i > -1 ) {
                        indAvgFatals.push([item[0], item[1], fatalsVal[i]]);
                      } else {
                        indAvgFatals.push([item[0], item[1], 0]);
                      }
                    });
                  }

                  oshaViolationsData.push({'key' : 'Industry Avg Fatal', 'values' :indAvgFatals});
                }

                if (data.overview.hasOwnProperty('ind_avg_nonfatal') &&
                  data.overview.ind_avg_nonfatal !== null) {

                  fatals = data.overview.ind_avg_nonfatal;
                  fatalsKey = [];
                  fatalsVal = [];

                  for (key in fatals) {
                    fatalsKey.push(key);
                    fatalsVal.push(fatals[key]);
                  }

                  if(fatals !== null) {

                    angular.forEach(quarters, function(item) {
                      var i = fatalsKey.indexOf( item[1].toString() );

                      if( i > -1 ) {
                        indAvgNonFatals.push([item[0], item[1], fatalsVal[i]]);
                      } else {
                        indAvgNonFatals.push([item[0], item[1], 0]);
                      }

                    });

                  }

                  oshaViolationsData.push({'key' : 'Industry Avg Non-fatal', 'values' :indAvgNonFatals});
                }

                var keyArray  = [];

                angular.forEach(oshaViolationsData, function(obj) {

                  angular.forEach(obj.values, function(item) {

                    if(!inArray([item[0], item[1]], keyArray)) {
                      keyArray.push( [item[0], item[1]] );
                    }

                  });

                });

                keyArray = sortData(keyArray);

                var indexData = 0;
                angular.forEach(oshaViolationsData, function(obj) {
                  var currentKeyArray = [];
                  angular.forEach(obj.values, function(item) {

                    currentKeyArray.push( [item[0], item[1]] );

                  });

                  angular.forEach(keyArray, function(item) {

                    if(!inArray(item, currentKeyArray)) {
                      obj.values.push( [ item[0], item[1], 0 ] );
                    }

                  });

                  obj.values = sortData(obj.values);
                  indexData ++;
                });
              }
            resolve(oshaViolationsData);
          })
          .catch(function (err) {
            reject(err);
          });
        });
      }

      function getBenchmarkingData() {
        function getArrayData(jsonData) {
          var arrayData = [];
          for( var i in jsonData ) {
              arrayData.push([i, jsonData[i]]);
          }
          return arrayData;
        }

        return $q(function (resolve, reject) {
          var injuryAndIllness = [
            {'key' : 'tcr', 'values' : ''},
            {'key' : 'industry_tcr', 'values' : ''},
            {'key' : 'dafw', 'values' : ''},
            {'key' : 'industry_dafw', 'values' : ''}
          ];

           getOverview($rootScope.getIntakeId(), $rootScope.getCompanyId())
              .then(function (data) {
                if ( Object.getOwnPropertyNames(data).length <= 0)  {
                   reject('Overview data is not popiulated!');
                   return;
                }
                else {

                  if (typeof data.overview === "undefined" || data.overview === null) {
                    reject('Overview is undefined.');
                    return;
                  }

                  if (typeof data.overview.DAFW_INDUSTRY == "undefined") {
                    reject('DAFW is undefined.');
                    return;
                  }

                  var dafw_industry = []; //tcr_company = [], , dafw_company = [], tcr_industry = [], non_total = [];

                  var dafwArray = getArrayData(data.overview.DAFW_INDUSTRY);
                  angular.forEach(dafwArray, function(item) {
                    dafw_industry.push([item[1].year, item[1].value]);
                  });

                  dafw_industry.sort(function(a, b){
                    return parseInt(a[0]) - parseInt(b[0]);
                  });

                  injuryAndIllness = [
                    // {'key' : 'TCR', 'values' : tcr_company},
                    // {'key' : 'Industry TCR', 'values' : tcr_industry},
                    // {'key' : 'DAFW', 'values' : dafw_company},
                    {'key' : 'Industry DAFW', 'values' : dafw_industry}
                    // {'key' : 'Total DAFW', 'values' : non_total}
                  ];

                  resolve(injuryAndIllness);
                }
              });
        });
      }

      function getMedicalCostsData() {
        return $q(function (resolve, reject) {
          getOverview($rootScope.getIntakeId(), $rootScope.getCompanyId())
            .then(function (data) {
              if (!data || !data.overview || !data.overview.medical_costs) {
                reject('No medical_costs information in overview!');
                return;
              }

              resolve(data.overview.medical_costs);
            })
            .catch(function (err) {
              reject(err);
            });
        });
      }

      function getVehiclesData() {
        return $q(function (resolve, reject) {
          getOverview($rootScope.getIntakeId(), $rootScope.getCompanyId())
            .then(function (data) {
              if (!data || !data.overview || !data.overview.vehicles_drivers) {
                reject('No vehicles_drivers information in overview!');
                return;
                // data.overview.vehicles_drivers = {
                //   "totalDrivers": 1234,
                //   "averageMilesDriven": 12651,
                //   "nationalAverageMilesDriven": 8851
                // };
              }

              resolve(data.overview.vehicles_drivers);
            })
            .catch(function (err) {
              reject(err);
            });
        });
      }
    }]);
})();