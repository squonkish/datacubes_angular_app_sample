/* global angular */
(function () {
  'use strict';
  angular.module('mvpUiApp')
.controller('benchmarkingCtrl', ['$rootScope', '$scope', 'Risk360', 'riskAnalysisModalOptions', 'errorService',
  function($rootScope, $scope, Risk360, riskAnalysisModalOptions, errorService) {
    $scope.modalOptions = riskAnalysisModalOptions;

    Risk360.getOverview($rootScope.getIntakeId(), $rootScope.getCompanyId())
      .then(function (data) {
        $scope.overview = data.overview;

        if ($scope.overview.TCR_INDUSTRY && angular.isArray($scope.overview.TCR_INDUSTRY)) {
          $scope.overview.TCR_INDUSTRY = $scope.overview.TCR_INDUSTRY[0];
        }
      })
      .catch(function (err) {
        errorService.logError(err);
      });

    Risk360.getBenchmarkingData()
      .then(function (data) {
          $scope.data = data;

          $scope.options = {
              chart: {
                  type: 'lineChart',
                  height: 250,
                  margin : {
                      top: 20,
                      right: 20,
                      bottom: 40,
                      left: 55
                  },
                  tooltip: {
                      keyFormatter: function(d) {
                          return d;
                      }
                  },
                  interpolate: 'cardinal',
                  lines: {forceY:[0]},
                  color: ['#7777ff', '#3b3b7f', '#FFB732', '#e55c00'],
                  x: function(d){ return d[0]; },
                  y: function(d){ return d[1]; },
                  //useInteractiveGuideline: true,
                  /*dispatch: {
                      stateChange: function(e){ console.log("stateChange"); },
                      changeState: function(e){ console.log("changeState"); },
                      tooltipShow: function(e){ console.log("tooltipShow"); },
                      tooltipHide: function(e){ console.log("tooltipHide"); }
                  },*/
                  xAxis: {
                      tickFormat: function(d){

                          // var year = $scope.data[0].values[d].x|| 0;
                          // var quater = $scope.data[0].values[d].z|| 0;
                          // if( quater == 0 ) quater = 1;
                          // var dx = "Q" + quater;
                          // dx += '-';
                          // dx += year;
                          return d;
                          // var label = $scope.data[0].values[d].z;
                          // return label;
                      }
                  },
                  yAxis: {
                      axisLabel: '',
                      tickFormat: function(d){
                          return d;
                      },
                      axisLabelDistance: 0
                  },
                  callback: function(/* chart */){
                      //console.log("!!! lineChart callback !!!");
                  }
              },
              title: {
                  enable: true,
                  text: 'Benchmarking'
              },
              subtitle: {
                  enable: false,
                  //text: 'Subtitle for simple line chart. Lorem ipsum dolor sit amet, at eam blandit sadipscing, vim adhuc sanctus disputando ex, cu usu affert alienum urbanitas.',
                  css: {
                      'text-align': 'center',
                      'margin': '10px 13px 0px 7px'
                  }
              },
              caption: {
                  enable:false,
                  //html: '<b>Figure 1.</b> Lorem ipsum dolor sit amet, at eam blandit sadipscing, <span style="text-decoration: underline;">vim adhuc sanctus disputando ex</span>, cu usu affert alienum urbanitas. <i>Cum in purto erat, mea ne nominavi persecuti reformidans.</i> Docendi blandit abhorreant ea has, minim tantas alterum pro eu. <span style="color: darkred;">Exerci graeci ad vix, elit tacimates ea duo</span>. Id mel eruditi fuisset. Stet vidit patrioque in pro, eum ex veri verterem abhorreant, id unum oportere intellegam nec<sup>[1, <a href="https://github.com/krispo/angular-nvd3" target="_blank">2</a>, 3]</sup>.',
                  css: {
                      'text-align': 'justify',
                      'margin': '10px 13px 0px 7px'
                  }
              }
          };
      });
    }]);
})();