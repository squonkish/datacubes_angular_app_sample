/* global angular */
(function () {
  'use strict';

  angular
    .module('mvpUiApp')
    .directive('benchmarkingSummary', function () {
      return {
        templateUrl: 'app/underwriting/risk360/benchmarking/benchmarkingTemplate.html',
        restrict: 'E',
        controller: 'benchmarkingCtrl',
        scope: {}
      };
    });
})();